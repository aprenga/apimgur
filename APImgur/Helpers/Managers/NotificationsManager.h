//
//  NotificationsManager.h
//  APImgur
//
//  Created by Aleksander Prenga on 4/7/16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "APSectionHotViewController.h"
#import "APSectionTopViewController.h"

@interface NotificationsManager : NSObject

+(void)registerSectionHotControllerLocalNotification:(APSectionHotViewController*)controller;
+(void)registerSectionTopControllerLocalNotification:(APSectionTopViewController*)controller;
+(void)postNotificationForViewTypeChanging;
+(void)postNotificationForSettingsChanging;

@end
