//
//  NotificationsManager.m
//  APImgur
//
//  Created by Aleksander Prenga on 4/7/16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import "NotificationsManager.h"

@implementation NotificationsManager

#pragma mark - Register Notifications

+(void)registerSectionHotControllerLocalNotification:(APSectionHotViewController*)controller {
    [[NSNotificationCenter defaultCenter] addObserver:controller selector:@selector(reloadCollectionView) name:@"viewTypeChanged" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:controller selector:@selector(reloadCollectionDataContents) name:@"settingsChanged" object:nil];
}

+(void)registerSectionTopControllerLocalNotification:(APSectionTopViewController*)controller {
    [[NSNotificationCenter defaultCenter] addObserver:controller selector:@selector(reloadCollectionView) name:@"viewTypeChanged" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:controller selector:@selector(reloadCollectionDataContents) name:@"settingsChanged" object:nil];
}

#pragma mark - Post Notifications

+(void)postNotificationForViewTypeChanging {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"viewTypeChanged" object:nil];
}

+(void)postNotificationForSettingsChanging {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"settingsChanged" object:nil];
}


@end
