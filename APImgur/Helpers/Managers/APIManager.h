//
//  APIManager.h
//  APImgur
//
//  Created by Alex on 4/6/16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Macros.h"
#import "APPosts.h"

@interface APIManager : NSObject

-(void)getGalleryImagesWithSectionType:(SectionType)sectionType page:(NSInteger)pageNumber completionBlock:(void (^)(BOOL success, NSString* error, APPosts* responseArray))completionFinished;

@end
