//
//  APIManager.m
//  APImgur
//
//  Created by Alex on 4/6/16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import "APIManager.h"
#import <AFNetworking.h>
#import "URLs.h"
#import "Preferences.h"
#import "APPostModel.h"


@implementation APIManager

-(void)getGalleryImagesWithSectionType:(SectionType)sectionType page:(NSInteger)pageNumber completionBlock:(void (^)(BOOL success, NSString* error, APPosts* responseArray))completionFinished {
    //Check network reachability
    if ([AFNetworkReachabilityManager sharedManager].reachable) {
        NSString *buildUrl = kBaseUrl;
        buildUrl = [buildUrl stringByAppendingPathComponent:[Macros getSectionFromType:sectionType]];
        buildUrl = [buildUrl stringByAppendingPathComponent:[Macros getSortFromType:[Preferences getSortType]]];
        if (sectionType == SectionTypeTop) {
            buildUrl = [buildUrl stringByAppendingPathComponent:[Macros getWindowFromType:[Preferences getWindowType]]];
        }
        if (sectionType == SectionTypeUser) {
            NSString *showViral = [Preferences getShouldIncludeViral] ? @"true" : @"false";
            buildUrl = [buildUrl stringByAppendingString:[NSString stringWithFormat:@"?showViral=%@", showViral]];
        }
        buildUrl = [buildUrl stringByAppendingPathComponent:@"0"]; //Page
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager.requestSerializer setValue:kImgurClientID forHTTPHeaderField:@"client_id"];
        [manager GET:buildUrl parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
            APPosts *posts = [[APPosts alloc] initWithDictionary:responseObject error:nil];
            completionFinished(true, @"", posts);
        } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
            APPosts *posts = [[APPosts alloc] init];
            APPostModel *postModel = posts.data[0];
            completionFinished(false, postModel.error, posts);
        }];
    }
}

@end
