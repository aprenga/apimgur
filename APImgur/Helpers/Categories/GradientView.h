//
//  GradientView.h
//  Classroom
//
//  Created by Alex on 2/11/16.
//  Copyright © 2016 Skooli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GradientView : UIView

@property (nonatomic, strong, readonly) CAGradientLayer *layer;

@end
