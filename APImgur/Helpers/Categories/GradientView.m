//
//  GradientView.m
//  Classroom
//
//  Created by Alex on 2/11/16.
//  Copyright © 2016 Skooli. All rights reserved.
//

#import "GradientView.h"

@implementation GradientView

//@synthesize layer = _layer;

+ (Class)layerClass {
    return [CAGradientLayer class];
}

@end
