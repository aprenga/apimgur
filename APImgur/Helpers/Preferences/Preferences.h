//
//  Preferences.h
//  APImgur
//
//  Created by Alex on 4/6/16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Macros.h"

@interface Preferences : NSObject

+(ViewType)getGalleryViewType;
+(void)setGalleryViewType:(ViewType)viewType;

+(BOOL)getShouldIncludeViral;
+(void)setShouldIncludeViral:(BOOL)shouldIncludeViral;

+(SortType)getSortType;
+(void)setSortType:(SortType)sortType;

+(WindowType)getWindowType;
+(void)setWindowType:(WindowType)windowType;

@end
