//
//  Utilities.h
//  APImgur
//
//  Created by Alex on 4/6/16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Utilities : NSObject

+(UIColor*)mainBlueColor;

@end
