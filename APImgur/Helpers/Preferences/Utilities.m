//
//  Utilities.h
//  APImgur
//
//  Created by Alex on 4/6/16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import "Utilities.h"

@implementation Utilities

+(UIColor*)mainBlueColor {
    return [UIColor colorWithRed:(3/255.f) green:(56/255.f) blue:(134/255.f) alpha:1];
}

@end
