//
//  Preferences.m
//  APImgur
//
//  Created by Alex on 4/6/16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import "Preferences.h"

@implementation Preferences {
}


#pragma mark - View type
//Here we get and set the type of the view that user has chosen (list, grid, staggered grid)

+(ViewType)getGalleryViewType {
    return [[NSUserDefaults standardUserDefaults] integerForKey:@"viewType"];
}

+(void)setGalleryViewType:(ViewType)viewType {
    [[NSUserDefaults standardUserDefaults] setInteger:viewType forKey:@"viewType"];
}

#pragma mark - Should Include/Exclude Viral
// Show or hide viral images from the 'user' section

+(BOOL)getShouldIncludeViral {
    return ([[NSUserDefaults standardUserDefaults] boolForKey:@"includeViral"]) ? true : false;
}

+(void)setShouldIncludeViral:(BOOL)shouldIncludeViral {
    [[NSUserDefaults standardUserDefaults] setBool:shouldIncludeViral forKey:@"includeViral"];
}

#pragma mark - Sort Types
// (only available with user section) - defaults to viral

+(SortType)getSortType {
    return [[NSUserDefaults standardUserDefaults] integerForKey:@"sortType"];
}

+(void)setSortType:(SortType)sortType {
    [[NSUserDefaults standardUserDefaults] setInteger:sortType forKey:@"sortType"];
}

#pragma mark - Window Type
// Change the date range of the request if the section is "top"

+(WindowType)getWindowType {
    return [[NSUserDefaults standardUserDefaults] integerForKey:@"windowType"];
}

+(void)setWindowType:(WindowType)windowType {
    [[NSUserDefaults standardUserDefaults] setInteger:windowType forKey:@"windowType"];
}

@end
