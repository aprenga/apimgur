//
//  Macros.h
//  APImgur
//
//  Created by Alex on 4/6/16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Macros : NSObject

typedef NS_ENUM(NSInteger, ViewType) {
    ViewTypeList,
    ViewTypeGrid,
    ViewTypeStaggeredGrid
};

typedef NS_ENUM(NSInteger, SectionType) {
    SectionTypeHot,
    SectionTypeTop,
    SectionTypeUser
};

typedef NS_ENUM(NSInteger, SortType) {
    SortTypeViral,
    SortTypeTop,
    SortTypeTime,
    SortTypeRising
};

typedef NS_ENUM(NSInteger, WindowType) {
    WindowTypeDay,
    WindowTypeWeek,
    WindowTypeMonth,
    WindowTypeYear,
    WindowTypeAll
};

typedef NS_ENUM(NSInteger, SettingsCellType) {
    SettingsCellTypeView,
    SettingsCellTypeSort,
    SettingsCellTypeWindow,
    SettingsCellTypeShowViral,
    SettingsCellTypeAbout
};

+(CGSize)getGalleryCellSizeForViewType:(ViewType)viewType;


+(NSString*)getViewFromType:(ViewType)type;
+(NSString*)getSectionFromType:(SectionType)type;
+(NSString*)getSortFromType:(SortType)type;
+(NSString*)getWindowFromType:(WindowType)type;

@end
