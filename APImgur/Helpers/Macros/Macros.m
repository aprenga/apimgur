//
//  Macros.m
//  APImgur
//
//  Created by Alex on 4/6/16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import "Macros.h"

@implementation Macros

//Return the CollectionView cell sizes depending on the type of view that the user has selected

+(CGSize)getGalleryCellSizeForViewType:(ViewType)viewType {
    CGSize size = [[UIScreen mainScreen] bounds].size;
    switch (viewType) {
        case ViewTypeList:
            return CGSizeMake(size.width - 10, size.width/2);
            break;
        case ViewTypeGrid:
            return CGSizeMake(size.width/2 - 4, size.width/2 - 4);
            break;
        case ViewTypeStaggeredGrid:
            return CGSizeMake(size.width/2 - 4, size.width/2 - 4);
            break;
        default:
            return CGSizeMake(size.width - 10, size.width/2); //Return list as default
            break;
    }
}

+(NSString *)getViewFromType:(ViewType)type {
    switch (type) {
        case ViewTypeList:
            return @"list";
            break;
        case ViewTypeGrid:
            return @"grid";
            break;
        case ViewTypeStaggeredGrid:
            return @"staggered grid";
            break;
        default:
            return @"list";
            break;
    }
}

+(NSString*)getSectionFromType:(SectionType)type {
    switch (type) {
        case SectionTypeHot:
            return @"hot";
            break;
        case SectionTypeTop:
            return @"top";
            break;
        case SectionTypeUser:
            return @"user";
            break;
        default:
            return @"hot";
            break;
    }
}

+(NSString*)getSortFromType:(SortType)type {
    switch (type) {
        case SortTypeViral:
            return @"viral";
            break;
        case SortTypeTop:
            return @"top";
            break;
        case SortTypeTime:
            return @"time";
            break;
        case SortTypeRising:
            return @"rising";
            break;
        default:
            return @"viral";
            break;
    }
}

+(NSString*)getWindowFromType:(WindowType)type {
    switch (type) {
        case WindowTypeDay:
            return @"day";
            break;
        case WindowTypeWeek:
            return @"week";
            break;
        case WindowTypeMonth:
            return @"month";
            break;
        case WindowTypeYear:
            return @"year";
            break;
        case WindowTypeAll:
            return @"all";
            break;
        default:
            return @"day";
            break;
    }
}



@end
