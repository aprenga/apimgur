//
//  APPosts.h
//  APImgur
//
//  Created by Alex on 4/7/16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"
#import "APPostModel.h"

@interface APPosts : JSONModel

@property (strong, nonatomic) NSArray<APPostModel>* data;

@end
