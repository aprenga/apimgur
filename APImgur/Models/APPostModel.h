//
//  APPostModel.h
//  APImgur
//
//  Created by Alex on 4/6/16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@protocol APPostModel @end

@interface APPostModel : JSONModel

@property (strong, nonatomic) NSString *link;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString<Optional>* description;
@property (assign) NSInteger ups;
@property (assign) NSInteger downs;
@property (assign) NSInteger score;
@property (strong, nonatomic) NSString<Optional>* error;


@end
