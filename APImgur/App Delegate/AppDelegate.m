//
//  AppDelegate.m
//  APImgur
//
//  Created by Alex on 4/5/16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import "AppDelegate.h"
#import "AFNetworkReachabilityManager.h"
#import "Utilities.h"
#import "Macros.h"
#import "Preferences.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    //Add Network Observer
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    //Setup navigation bar colors
    [self setupNavigationDetails];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Setups

//Setup navigation bar colors
-(void) setupNavigationDetails {
    //Percaktimi i ngjyres se barbuttons
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    //White navigation Titles
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    //Blue navigation background color
    [[UINavigationBar appearance] setBarTintColor:[Utilities mainBlueColor]];
    //Text Color
    [[UITabBar appearance] setTintColor:[UIColor whiteColor]];
    //Small hack for Background Color
    [[UITabBar appearance] setBackgroundImage:[UIImage new]];
    [[UITabBar appearance] setBackgroundColor:[Utilities mainBlueColor]];
    
    self.window.tintColor = [UIColor whiteColor];
}

@end
