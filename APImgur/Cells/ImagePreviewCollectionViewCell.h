//
//  ImagePreviewCollectionViewCell.h
//  APImgur
//
//  Created by Alex on 4/6/16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APPostModel.h"

@interface ImagePreviewCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIView *gradientView;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

//Public Methods
-(void)configureCellWithModel:(APPostModel*)model;

@end
