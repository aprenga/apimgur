//
//  ImagePreviewCollectionViewCell.m
//  APImgur
//
//  Created by Alex on 4/6/16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import "ImagePreviewCollectionViewCell.h"
#import "GradientView.h"
#import <Masonry/Masonry.h>
#import <UIActivityIndicator-for-SDWebImage/UIImageView+UIActivityIndicatorForSDWebImage.h>
#import "Utilities.h"
#import <QuartzCore/QuartzCore.h>

@implementation ImagePreviewCollectionViewCell

-(void)awakeFromNib {
    [self createLabelBackgroundGradientLayer];
    [self createCellRoundCorners];
}

#pragma mark - Setups

-(void)configureCellWithModel:(APPostModel*)model {
    [self.imageView setImageWithURL:[NSURL URLWithString:model.link] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.descriptionLabel.text = model.title;
}

-(void)createCellRoundCorners {
    self.layer.borderWidth = 1.0f;
    self.layer.borderColor = [Utilities mainBlueColor].CGColor;
    self.layer.cornerRadius = 5.0f;
    self.gradientView.layer.cornerRadius = 5.0f;
}

#pragma mark - Graphic Methods

//Add gradient color to the whiteboard background contur
-(void)createLabelBackgroundGradientLayer {
    //We have created our own gradient layer to be managed by a view. You do that by creating a UIView subclass that uses a CAGradientLayer as its layer. Now we can manage the gradient layer as uiview and we won't have issues with resizing or rotation
    GradientView *gradientMask = [[GradientView alloc] initWithFrame:self.gradientView.bounds];
    gradientMask.frame = self.gradientView.bounds;
    NSArray *gradientColors = [NSArray arrayWithObjects:(id)[UIColor colorWithWhite:0.5 alpha:0.1].CGColor, (id)[UIColor colorWithWhite:0 alpha:0.5].CGColor, nil];
    gradientMask.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    gradientMask.layer.colors = gradientColors;
    gradientMask.layer.startPoint = CGPointMake(0.5, 0.0);   // start at left up
    gradientMask.layer.endPoint = CGPointMake(0.5, 1.0);     // end at right down
    [self.gradientView addSubview:gradientMask];
    [self.gradientView sendSubviewToBack:gradientMask];
    self.gradientView.backgroundColor = [UIColor clearColor];
}

@end
