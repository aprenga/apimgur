//
//  SettingsTableViewCell.m
//  APImgur
//
//  Created by Alex on 4/7/16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import "SettingsTableViewCell.h"
#import "Macros.h"
#import "Preferences.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "NotificationsManager.h"

@implementation SettingsTableViewCell

-(void)awakeFromNib {
    
}

#pragma mark - Setups {

-(void)configureCellWithIndex:(NSInteger)index {
    switch (index) {
        case SettingsCellTypeView:
            [self setupViewTypeCell];
            break;
        case SettingsCellTypeSort:
            [self setupSortTypeCell];
            break;
        case SettingsCellTypeWindow:
            [self setupWindowTypeCell];
            break;
        case SettingsCellTypeShowViral:
            [self setupShowViralCell];
            break;
        case SettingsCellTypeAbout:
            [self setupAboutCell];
            break;
        default:
            break;
    }
}

#pragma mark - Data Binding

-(void)setupViewTypeCell {
    self.titleLabel.text = @"View Type";
    self.descriptionLabel.text = [[Macros getViewFromType:[Preferences getGalleryViewType]] capitalizedString];
}

-(void)setupSortTypeCell {
    self.titleLabel.text = @"Sort Type";
    self.descriptionLabel.text = [[Macros getSortFromType:[Preferences getSortType]] capitalizedString];
}

-(void)setupWindowTypeCell {
    self.titleLabel.text = @"Window Type";
    self.descriptionLabel.text = [[Macros getWindowFromType:[Preferences getWindowType]] capitalizedString];
}

-(void)setupShowViralCell {
    self.titleLabel.text = @"Show Viral";
    [self setupReactiveSwitch];
    [self.viralSwitch setOn:[Preferences getShouldIncludeViral] animated:true];
}

-(void)setupAboutCell {
    self.titleLabel.text = @"About";
    self.descriptionLabel.text = @"";
}

#pragma mark - Reactive Subscribers

-(void)setupReactiveSwitch {
    [self.viralSwitch.rac_newOnChannel subscribeNext:^(id x) {
        [Preferences setShouldIncludeViral:x];
        [NotificationsManager postNotificationForSettingsChanging];
    }];
}


@end
