//
//  APSectionHotViewController.h
//  APImgur
//
//  Created by Alex on 4/6/16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImagePreviewCollectionViewCell.h"
#import "APPosts.h"
#import <IDMPhotoBrowser/IDMPhotoBrowser.h>
#import "CHTCollectionViewWaterfallLayout.h"

@interface APSectionHotViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, IDMPhotoBrowserDelegate, CHTCollectionViewDelegateWaterfallLayout>

//Outlets
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

//Needed properties
@property (strong, nonatomic) APPosts *dataArray; //Store models of imgur objects

//Public Methods
-(void)reloadCollectionView;
-(void)reloadCollectionDataContents;

@end
