//
//  APSectionTopViewController.h
//  APImgur
//
//  Created by Alex on 4/7/16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImagePreviewCollectionViewCell.h"
#import "APPosts.h"
#import <IDMPhotoBrowser/IDMPhotoBrowser.h>

@interface APSectionTopViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, IDMPhotoBrowserDelegate>

//Outlets
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

//Needed properties
@property (strong, nonatomic) APPosts *dataArray; //Store models of imgur objects

//Public Methods
-(void)reloadCollectionView;
-(void)reloadCollectionDataContents;

@end
