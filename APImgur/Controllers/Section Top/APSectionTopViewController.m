//
//  APSectionTopViewController.m
//  APImgur
//
//  Created by Alex on 4/7/16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import "APSectionTopViewController.h"
#import "Preferences.h"
#import "Macros.h"
#import "CustomCollectionFlowLayout.h"
#import "APIManager.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "NotificationsManager.h"

@implementation APSectionTopViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //Setup the needed details of the collectionView
    [self setupCollectionView];
    
    [self initialSetups];
    
    //Get the HOT section images gallery
    [self getSectionHotGallery];
}

-(void)viewDidAppear:(BOOL)animated {
    self.tabBarController.navigationItem.title = @"Top";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Setups

-(void)initialSetups {
    _dataArray = [[APPosts alloc] init];
    //Register class for local notifications
    [NotificationsManager registerSectionTopControllerLocalNotification:self];
}

//Setup the needed details of the collectionView
-(void)setupCollectionView {
    // Allocate and configure the layout.
    CustomCollectionFlowLayout *layout = [[CustomCollectionFlowLayout alloc] init];
    layout.minimumInteritemSpacing = 5.f;
    layout.minimumLineSpacing = 5.f;
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    self.collectionView.collectionViewLayout = layout;
    self.collectionView.backgroundColor = [UIColor whiteColor];
}

#pragma mark - CollectionView Delegate & Datasource

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _dataArray.data.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ImagePreviewCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];;
    APPostModel *model = _dataArray.data[indexPath.row];
    [cell configureCellWithModel:model];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self openImageBrowserWithIndex:indexPath.row];
}

#pragma mark - FlowLayout Delegate Methods

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return [Macros getGalleryCellSizeForViewType:[Preferences getGalleryViewType]];
}

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 5;
}

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 5;
}

#pragma mark - Action Methods

//Get the HOT section images gallery
-(void)getSectionHotGallery {
    //Create Progress Bar Hud with Spinner
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [hud setLabelText:@"Loading Gallery..."];
    hud.removeFromSuperViewOnHide = YES;
    //Delay the auto login operation by 1 seccond in order for the AFNetworking to create the Network Manager shared instance
    double delayInSeconds = 1;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        APIManager *apiManager = [[APIManager alloc] init];
        [apiManager getGalleryImagesWithSectionType:SectionTypeTop page:0 completionBlock:^(BOOL success, NSString *error, APPosts *responseArray) {
            if (success) {
                _dataArray = responseArray;
                [_collectionView reloadData];
            } else {
                [self createAlertWithTitle:error];
            }
            hud.hidden = true;
        }];
    });
}

#pragma mark - Alerts

-(void)createAlertWithTitle:(NSString*)errorDescription {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:errorDescription preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    UIAlertAction *retryAction = [UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self getSectionHotGallery];
    }];
    
    [alertController addAction:cancelAction];
    [alertController addAction:retryAction];
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        [alertController setModalPresentationStyle:UIModalPresentationPopover];
        UIPopoverPresentationController *popPresenter = [alertController popoverPresentationController];
        popPresenter.sourceView = self.view;
        popPresenter.sourceRect = self.view.bounds;
        [self presentViewController:alertController animated:YES completion:nil];
    } else {
        [self presentViewController:alertController animated:YES completion:nil];
    }
    alertController.view.tintColor = [UIColor blueColor];
}

#pragma mark - Photo Browser Methods

-(void)openImageBrowserWithIndex:(NSUInteger)index {
    // Create an array to store IDMPhoto objects
    NSMutableArray *photos = [NSMutableArray new];
    IDMPhoto *photo = [[IDMPhoto alloc] init];
    for (APPostModel *model in _dataArray.data) {
        photo = [IDMPhoto photoWithURL:[NSURL URLWithString:model.link]];
        photo.caption = model.description;
        [photos addObject:photo];
    }
    //Create the Photo Browser Controller
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:photos];
    browser.delegate = self;
    browser.displayCounterLabel = true;
    browser.displayActionButton = true;
    [browser setInitialPageIndex:index];
    [self presentViewController:browser animated:true completion:nil];
}

#pragma mark - Helper Methods

#pragma mark - Public Methods

-(void)reloadCollectionView {
    [_collectionView reloadData];
}

-(void)reloadCollectionDataContents {
    [self getSectionHotGallery];
}


@end
