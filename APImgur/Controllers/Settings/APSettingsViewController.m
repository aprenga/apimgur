//
//  APSettingsViewController.m
//  APImgur
//
//  Created by Alex on 4/7/16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import "APSettingsViewController.h"
#import "Macros.h"
#import "APSettingsDetailsViewController.h"

@implementation APSettingsViewController {
}

-(void)viewDidLoad {
    [self initialSetups];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.tabBarController.navigationItem.title = @"Settings";
    [_tableView reloadData];
}

#pragma mark - Setups

-(void)initialSetups {
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

#pragma mark - UITableView Delegate & Datasource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return SettingsCellTypeAbout+1; //The last index + 1
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = (indexPath.row == SettingsCellTypeShowViral) ? @"switchCell" : @"viewTypeCell";
    SettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    [cell configureCellWithIndex:indexPath.row];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    if (indexPath.row != SettingsCellTypeShowViral && indexPath.row != SettingsCellTypeAbout) {
        APSettingsDetailsViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"settingsDetails"];
        vc.cellType = indexPath.row;
        [self.navigationController pushViewController:vc animated:true];
    }
}

#pragma mark - Segue Methods

//-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    if ([[segue identifier] isEqualToString:@"settingsDetailsSegue"]) {
//        APSettingsDetailsViewController *vc = [segue destinationViewController];
//        vc.cellType = setting
//    }
//}


@end
