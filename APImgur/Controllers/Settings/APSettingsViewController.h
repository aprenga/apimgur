//
//  APSettingsViewController.h
//  APImgur
//
//  Created by Alex on 4/7/16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SettingsTableViewCell.h"

@interface APSettingsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
