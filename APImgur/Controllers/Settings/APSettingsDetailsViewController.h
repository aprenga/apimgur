//
//  APSettingsDetailsViewController.h
//  APImgur
//
//  Created by Aleksander Prenga on 4/7/16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Macros.h"

@interface APSettingsDetailsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (assign) SettingsCellType cellType;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
