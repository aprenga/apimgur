//
//  APSettingsDetailsViewController.m
//  APImgur
//
//  Created by Aleksander Prenga on 4/7/16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import "APSettingsDetailsViewController.h"
#import "Utilities.h"
#import "Preferences.h"
#import "NotificationsManager.h"

@interface APSettingsDetailsViewController () {
    NSMutableArray* dataArray;
}

@end

@implementation APSettingsDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupHardcodedData];
    [self initialSetups];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Initial Setup

-(void)initialSetups {
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

-(void)setupHardcodedData {
    NSString *titleString;
    switch (_cellType) {
        case SettingsCellTypeView:
            dataArray = [[NSMutableArray alloc] initWithObjects:@"List", @"Grid", @"Staggered Grid", nil];
            titleString = @"View Type";
            break;
        case SettingsCellTypeSort:
            dataArray = [[NSMutableArray alloc] initWithObjects:@"Viral", @"Top", @"Time", @"Rising", nil];
            titleString = @"Sort Type";
            break;
        case SettingsCellTypeWindow:
            dataArray = [[NSMutableArray alloc] initWithObjects:@"Day", @"Week", @"Month", @"Year", @"All", nil];
            titleString = @"Window Type";
            break;
        default:
            break;
    }
    self.tabBarController.navigationItem.title = titleString;
    [self.tableView reloadData];
}

#pragma mark - UITableView Delegate & DataSource 

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.textLabel.text = dataArray[indexPath.row];
    [cell setTintColor:[Utilities mainBlueColor]];
    if ([self shouldDisplayAccessoryTypeAtIndexRow:indexPath.row]) {
        [tableView selectRowAtIndexPath:indexPath animated:true scrollPosition:UITableViewScrollPositionTop];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    [self saveSelectedData:indexPath.row];
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryNone;
}

#pragma mark - Data Binding

-(BOOL)shouldDisplayAccessoryTypeAtIndexRow:(NSInteger)indexRow {
    switch (_cellType) {
        case SettingsCellTypeView:
            return [[dataArray objectAtIndex:indexRow] isEqualToString:[[Macros getViewFromType:[Preferences getGalleryViewType]] capitalizedString]] ? true : false;
            break;
        case SettingsCellTypeSort:
            return [[dataArray objectAtIndex:indexRow] isEqualToString:[[Macros getSortFromType:[Preferences getSortType]] capitalizedString]] ? true : false;
            break;
        case SettingsCellTypeWindow:
            return [[dataArray objectAtIndex:indexRow] isEqualToString:[[Macros getWindowFromType:[Preferences getWindowType]] capitalizedString]] ? true : false;
            break;
        default:
            return false;
            break;
    }
}

#pragma mark - Data Setting

-(void)saveSelectedData:(NSInteger)indexRow {
    switch (_cellType) {
        case SettingsCellTypeView:
            [Preferences setGalleryViewType:indexRow];
            [NotificationsManager postNotificationForViewTypeChanging];
            break;
        case SettingsCellTypeSort:
            [Preferences setSortType:indexRow];
            [NotificationsManager postNotificationForSettingsChanging];
            break;
        case SettingsCellTypeWindow:
            [Preferences setWindowType:indexRow];
            [NotificationsManager postNotificationForSettingsChanging];
            break;
        default:
            break;
    }
}




@end
