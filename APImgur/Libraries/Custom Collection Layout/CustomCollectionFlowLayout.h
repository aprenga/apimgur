//
//  CustomCollectionFlowLayout.h
//  Cafe-Online
//
//  Created by Alex' on 8/25/15.
//  Copyright (c) 2015 Cafe Online. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCollectionFlowLayout : UICollectionViewFlowLayout

- (void)makeBoring;

@end
